/*
* MX1508.cpp
*
* Created: 03-Aug-18 19:37:43
* Author: https://moja-elka.blogspot.com
*/

#ifndef __MX1508_H__
#define __MX1508_H__

#include <Arduino.h>

#define DEFAULT_SPEED       255

class MX1508
{
    public:
        MX1508(int in1Pin, int in2Pin);
        void drive(int speed);
        void drive(int speed, int duration);
        void brake();
        void standby();
    protected:
    private:
        int in1, in2;
        void fwd(int speed);
        void rev(int speed);
};

void forward(MX1508 motor1, MX1508 motor2, int speed);
void forward(MX1508 motor1, MX1508 motor2);
void backward(MX1508 motor1, MX1508 motor2, int speed);
void backward(MX1508 motor1, MX1508 motor2);
void left(MX1508 left, MX1508 right, int speed, int withBothMotors);
void right(MX1508 left, MX1508 right, int speed, int withBothMotors);
void brake(MX1508 motor1, MX1508 motor2);
void standby(MX1508 motor1, MX1508 motor2);

#endif //__MX1508_H__

