#include "RC_Boat.h"
#include "MX1508.h"
#include <EnableInterrupt.h>

#define SERIAL_PORT_SPEED 9600
#define RC_NUM_CHANNELS  3

#define RC_CH1  0   //steering wheel/kierunek => max left value: 864 - center: 1420 - max right: 2016
#define RC_CH2  1   //throttle/przepustnica => center: 1500, max forward: 1736, max backward: 1284
#define RC_CH3  2   //button AUX => off: 1104, on: 1968

#define RC_CH1_INPUT  A0
#define RC_CH2_INPUT  A1
#define RC_CH3_INPUT  A2

#define THROTTLE_MIN            1284    //backward range: 1284 - 1450
#define THROTTLE_DEAD_ZONE_MIN  1450    //stop range: 1450 - 1550
#define THROTTLE_DEAD_ZONE_MAX  1550
#define THROTTLE_MAX            1760    //forward range: 1550 - 1736

#define STEERING_MIN            864     //turn left range: 864 - 1370
#define STEERING_DEAD_ZONE_MIN  1370    //straight range: 1370 - 1470
#define STEERING_DEAD_ZONE_MAX  1470
#define STEERING_MAX            2016    //turn right range: 1470 - 2016

#define SPEED_ZERO          0
#define DIRECTION_NONE      0
#define DIRECTION_FORWARD   1
#define DIRECTION_BACKWARD  2
#define TURN_NONE           0
#define TURN_LEFT           1
#define TURN_RIGHT          2

#define DC_PWM_MIN 100
#define DC_PWM_MAX 255

#define IN1 9
#define IN2 10
#define IN3 5
#define IN4 6

#define TURN_WITH_BOTH_MOTORS   0
#define SPEED_ZERO              0

//create a motor instance
MX1508 leftMotor = MX1508(IN1, IN2);
MX1508 rightMotor = MX1508(IN3, IN4);

uint16_t rcValues[RC_NUM_CHANNELS];
uint32_t rcStart[RC_NUM_CHANNELS];
volatile uint16_t rcShared[RC_NUM_CHANNELS];

uint16_t steering;
byte throttleDirection = 0;
byte boatIsStopped = 1;

void rcReadValues() {
    noInterrupts();
    memcpy(rcValues, (const void *)rcShared, sizeof(rcShared));
    interrupts();
}

void calcInput(uint8_t channel, uint8_t input_pin) {
    if (digitalRead(input_pin) == HIGH) {
        rcStart[channel] = micros();
    } else {
        uint16_t rcCompare = (uint16_t)(micros() - rcStart[channel]);
        rcShared[channel] = rcCompare;
    }
}

void calcCh1() { calcInput(RC_CH1, RC_CH1_INPUT); }
void calcCh2() { calcInput(RC_CH2, RC_CH2_INPUT); }
void calcCh3() { calcInput(RC_CH3, RC_CH3_INPUT); }

void setup() {
    //Serial.begin(9600);

    pinMode(RC_CH1_INPUT, INPUT);
    pinMode(RC_CH2_INPUT, INPUT);
    pinMode(RC_CH3_INPUT, INPUT);

    enableInterrupt(RC_CH1_INPUT, calcCh1, CHANGE);
    enableInterrupt(RC_CH2_INPUT, calcCh2, CHANGE);
    enableInterrupt(RC_CH3_INPUT, calcCh3, CHANGE);
    motorsStandby();
}

void loop()
{
    RC rcValues = getRcValues();

    if (0 == rcValues.speed && boatIsStopped == 0) {
        boatIsStopped = 1;
        motorsStop();
        delay(50);
    }

    if (rcValues.speed > 0) {
        if (boatIsStopped == 1) {
            boatIsStopped = 0;
        }

        if (rcValues.speed > 255) {
            rcValues.speed = 255;
        }

        if (DIRECTION_FORWARD == rcValues.direction) {
            switch (rcValues.turn) {
                case TURN_NONE:
                    motorsForward(rcValues.speed);
                    break;
                case TURN_LEFT:
                     motorsTurnLeft(rcValues.speed);
                    break;
                case TURN_RIGHT:
                     motorsTurnRight(rcValues.speed);
                    break;
            }
        }

        if (DIRECTION_BACKWARD == rcValues.direction) {
            motorsBackward(rcValues.speed);
        }
        delay(50);
    }

    //Serial.print("CH1:"); Serial.print(rc_values[RC_CH1]); Serial.print("\t");
    //Serial.print("CH2:"); Serial.print(rc_values[RC_CH2]); Serial.print("\t");
    //Serial.print("CH3:"); Serial.print(rc_values[RC_CH3]); Serial.print("\t");

    delay(200);
}

RC getRcValues()
{
    rcReadValues();
    uint16_t steering = rcValues[RC_CH1];
    uint16_t throttle = rcValues[RC_CH2];

    /*Serial.print(F("steering: "));
    Serial.println(steering);
    Serial.print(F("throttle: "));
    Serial.println(throttle);*/

    //stop if transmitter is disabled
    if (steering == 0 && throttle == 0) {
        return {SPEED_ZERO, DIRECTION_NONE, TURN_NONE};
    }

    //stop
    if (throttle >= THROTTLE_DEAD_ZONE_MIN && throttle <= THROTTLE_DEAD_ZONE_MAX) {
        return {SPEED_ZERO, DIRECTION_NONE, TURN_NONE};
    }

    //straight ahead
    if (throttle > THROTTLE_DEAD_ZONE_MAX && steering >= STEERING_DEAD_ZONE_MIN && steering <= STEERING_DEAD_ZONE_MAX) {
        return {
            map(throttle, THROTTLE_DEAD_ZONE_MAX, THROTTLE_MAX, DC_PWM_MIN, DC_PWM_MAX),
            DIRECTION_FORWARD,
            TURN_NONE
        };
    }

    //turn left
    if (throttle > THROTTLE_DEAD_ZONE_MAX && steering < STEERING_DEAD_ZONE_MIN) {
        return {
            map(throttle, THROTTLE_DEAD_ZONE_MAX, THROTTLE_MAX, DC_PWM_MIN, DC_PWM_MAX),
            DIRECTION_FORWARD,
            TURN_LEFT
        };
    }

    //turn right
    if (throttle > THROTTLE_DEAD_ZONE_MAX && steering > STEERING_DEAD_ZONE_MAX) {
        return {
            map(throttle, THROTTLE_DEAD_ZONE_MAX, THROTTLE_MAX, DC_PWM_MIN, DC_PWM_MAX),
            DIRECTION_FORWARD,
            TURN_RIGHT
        };
    }

    //backward
    if (throttle < THROTTLE_DEAD_ZONE_MIN) {
        return {
            map(throttle, THROTTLE_DEAD_ZONE_MIN, THROTTLE_MIN, DC_PWM_MIN, DC_PWM_MAX),    //TODO check values
            DIRECTION_BACKWARD,
            TURN_NONE
        };
    }
}

void motorsForward(int speed)
{
    //Serial.print(F("boatForward: "));
    //Serial.println(speed);
    forward(leftMotor, rightMotor, speed);
}

void motorsBackward(int speed)
{
    //Serial.print(F("boatBackward: "));
   // Serial.println(speed);
    backward(leftMotor, rightMotor, speed);
}

void motorsTurnLeft(int speed)
{
    //Serial.print(F("boatLeft: "));
    //Serial.println(speed);
    left(leftMotor, rightMotor, speed, TURN_WITH_BOTH_MOTORS);
}

void motorsTurnRight(int speed)
{
    //Serial.print(F("boatRight: "));
    //Serial.println(speed);
    right(leftMotor, rightMotor, speed, TURN_WITH_BOTH_MOTORS);
}

void motorsStop()
{
    //Serial.println(F("boatStop"));
    brake(leftMotor, rightMotor);
}

void motorsStandby()
{
    //Serial.println(F("boatStandby"));
    standby(leftMotor, rightMotor);
}
