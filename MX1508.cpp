/*
* MX1508.cpp
*
* Created: 03-Aug-18 19:37:43
* Author: https://moja-elka.blogspot.com
*/

#include "MX1508.h"

MX1508::MX1508(int in1Pin, int in2Pin)
{
    in1 = in1Pin;
    in2 = in2Pin;

    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);
}

void MX1508::drive(int speed)
{
    if (speed >= 0) {
        fwd(speed);
    } else {
        rev(-speed);
    }
}

void MX1508::drive(int speed, int duration)
{
    drive(speed);
    delay(duration);
}

void MX1508::fwd(int speed)
{
    analogWrite(in1, speed);
    digitalWrite(in2, LOW);
}

void MX1508::rev(int speed)
{
    digitalWrite(in1, LOW);
    analogWrite(in2, speed);
}

void MX1508::brake()
{
    digitalWrite(in1, HIGH);
    digitalWrite(in2, HIGH);
}

void MX1508::standby()
{
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);
}

//TODO doda� klas� pojazdu

void forward(MX1508 motor1, MX1508 motor2, int speed)
{
    motor1.drive(speed);
    motor2.drive(speed);
}

void forward(MX1508 motor1, MX1508 motor2)
{
    motor1.drive(DEFAULT_SPEED);
    motor2.drive(DEFAULT_SPEED);
}

void backward(MX1508 motor1, MX1508 motor2, int speed)
{
    int temp = abs(speed);
    motor1.drive(-temp);
    motor2.drive(-temp);
}

void backward(MX1508 motor1, MX1508 motor2)
{
    motor1.drive(-DEFAULT_SPEED);
    motor2.drive(-DEFAULT_SPEED);
}

void left(MX1508 left, MX1508 right, int speed, int withBothMotors)
{
    if (1 == withBothMotors) {
        int temp = abs(speed)/2;
        left.drive(-temp);
        right.drive(temp);
        } else {
        left.drive(0);
        right.drive(abs(speed));
    }
}

void right(MX1508 left, MX1508 right, int speed, int withBothMotors)
{
    if (1 == withBothMotors) {
        int temp = abs(speed)/2;
        left.drive(temp);
        right.drive(-temp);
        } else {
        left.drive(abs(speed));
        right.drive(0);
    }
}

void brake(MX1508 motor1, MX1508 motor2)
{
    motor1.brake();
    motor2.brake();
}

void standby(MX1508 motor1, MX1508 motor2)
{
    motor1.standby();
    motor2.standby();
}