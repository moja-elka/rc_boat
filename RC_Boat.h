/*
 * RC_Boat.h
 *
 * Created: 07-Aug-18 22:36:17
 *  Author: tygrysek
 */

struct RC {
    int speed;  // vehicle speed
    byte direction; // vehicle direction: 0 stop, 1 forward, 2 backward
    byte turn;  // vehicle turn: 0 straight, 1 left, 2 right
};
